//
//  RectViewPresenter.swift
//  Rect
//
//  Created by Vadim Shalugin on 17.03.2021.
//

import UIKit

protocol RectangleViewPresenterDelegate: UIViewController {}

class RectangleViewPresenter {
    
    weak var viewController: RectangleViewPresenterDelegate!
    private var sideSize: CGFloat = 50
    private var rectangle: Rectangle?
    
    init(viewController: RectangleViewPresenterDelegate) {
        self.viewController = viewController
    }

    func drawRect() {
        rectangle = Rectangle(sideSize: sideSize, parentViewWidth: viewController.view.frame.width, parentViewHeight: viewController.view.frame.height)
        UIView.animate(withDuration: 1) {
            self.rectangle?.transform = CGAffineTransform(scaleX: 2, y: 2)
        }
        viewController.view.addSubview(rectangle!)
    }
}
